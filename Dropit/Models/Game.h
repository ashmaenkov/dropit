//
//  Game.h
//  Dropit
//
//  Created by Artsiom Shmaenkov on 9/13/17.
//  Copyright © 2017 neoviso. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    GameStateActive = 0,
    GameStateStopped = 1,
    GameStateFinished = 2
    
} typedef GameState;

@interface Game : NSObject

@property (nonatomic) GameState state;
@property (nonatomic, readonly) int score;
@property (nonatomic, readonly) int8_t level;

@end
