//
//  Game.m
//  Dropit
//
//  Created by Artsiom Shmaenkov on 9/13/17.
//  Copyright © 2017 neoviso. All rights reserved.
//

#import "Game.h"

@interface Game()

@property (nonatomic, readwrite) int score;
@property (nonatomic, readwrite) int8_t level;

@end

@implementation Game

@end
