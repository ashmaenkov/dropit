//
//  DropitBehavior.h
//  Dropit
//
//  Created by Artsiom Shmaenkov on 8/31/17.
//  Copyright © 2017 neoviso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropitBehavior : UIDynamicBehavior

-(void)addItem:(id<UIDynamicItem>)item;
-(void)removeItem:(id<UIDynamicItem>)item;

@end
