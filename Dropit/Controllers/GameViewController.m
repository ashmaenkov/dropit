//
//  GameViewController.m
//  Dropit
//
//  Created by Artsiom Shmaenkov on 9/14/17.
//  Copyright © 2017 neoviso. All rights reserved.
//

#import "GameViewController.h"
#import "DropitBehavior.h"
#import "GameAreaView.h"

@interface GameViewController () <UIDynamicAnimatorDelegate>

@property (weak, nonatomic) IBOutlet GameAreaView *gameView;
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) DropitBehavior *dropitBehavior;
@property (strong, nonatomic) UIAttachmentBehavior *attachmentBehavior;
@property (strong, nonatomic) UIView *droppingView;

@end

@implementation GameViewController

static const CGSize DROP_SIZE = {40, 40};

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    //    [self drop];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIDynamicAnimator*)animator {
    if(!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.gameView];
        _animator.delegate = self;
    }
    
    return _animator;
}

- (DropitBehavior*)dropitBehavior {
    if(!_dropitBehavior) {
        _dropitBehavior = [[DropitBehavior alloc] init];
        [self.animator addBehavior:_dropitBehavior];
    }
    
    return _dropitBehavior;
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self drop];
}

-(void)drop {
    CGRect frame;
    frame.origin = CGPointZero;
    frame.size = DROP_SIZE;
    
    int x = (arc4random() % (int)self.gameView.bounds.size.width) / DROP_SIZE.width;
    frame.origin.x = x * DROP_SIZE.width;
    
    UIView *dropView = [[UIView alloc] initWithFrame:frame];
    dropView.backgroundColor = [self randomColor];
    [self.gameView addSubview:dropView];
    
    self.droppingView = dropView;
    [self.dropitBehavior addItem:dropView];
    
    [self performSelector:@selector(drop) withObject:nil afterDelay:0.5];
}

-(UIColor*)randomColor {
    switch (arc4random()%5) {
        case 0: return [UIColor greenColor];
        case 1: return [UIColor blueColor];
        case 2: return [UIColor orangeColor];
        case 3: return [UIColor redColor];
        case 4: return [UIColor purpleColor];
        default: return [UIColor blackColor];
    }
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator {
    [self removeCompletedRows];
}

- (BOOL)removeCompletedRows {
    NSMutableArray *dropsToRemove = [[NSMutableArray alloc] init];
    
    for (CGFloat y = self.gameView.bounds.size.height - DROP_SIZE.height / 2; y > 0; y -= DROP_SIZE.height) {
        BOOL rowIsComplete = YES;
        NSMutableArray *dropsFound = [[NSMutableArray alloc] init];
        for (CGFloat x = DROP_SIZE.width / 2; x <= self.gameView.bounds.size.width - DROP_SIZE.width / 2; x += DROP_SIZE.width) {
            UIView *hitView = [ self.gameView hitTest:CGPointMake(x, y) withEvent:nil];
            if ([hitView superview] == self.gameView) {
                [dropsFound addObject:hitView];
            } else {
                rowIsComplete = NO;
                break;
            }
        }
        if (![dropsFound count]) break;
        if (rowIsComplete) [dropsToRemove addObjectsFromArray:dropsFound];
        
        if ([dropsToRemove count]) {
            for (UIView *view in dropsToRemove) {
                [self.dropitBehavior removeItem:view];
            }
            [self animateRemovingDrops:dropsToRemove];
            return YES;
        }
    }
    
    return NO;
}

- (void)animateRemovingDrops:(NSArray*)dropsToRemove {
    [UIView animateWithDuration:1.0 animations:^{
        for (UIView *drop in dropsToRemove) {
            int x = arc4random() % (int)(self.gameView.bounds.size.width * 5) - (int)self.gameView.bounds.size.width * 2;
            int y = self.gameView.bounds.size.height;
            drop.center = CGPointMake(x, -y);
        }
    } completion:^(BOOL finished){
        [dropsToRemove makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }];
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    CGPoint gesturePoint = [sender locationInView:self.gameView];
    if(sender.state == UIGestureRecognizerStateBegan) {
//        [self attachDroppingViewToPoint:gesturePoint];
    }else if (sender.state == UIGestureRecognizerStateChanged) {
        self.attachmentBehavior.anchorPoint = gesturePoint;
        
    }else if (sender.state == UIGestureRecognizerStateEnded) {
        [self.animator removeBehavior:self.attachmentBehavior];
    }
}

//-(void)attachDroppingViewToPoint:(CGPoint)anchorPoint {
//    if (self.droppingView) {
//        self.attachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:self.droppingView attachedToAnchor:anchorPoint];
//        __weak typeof(self) weakSelf = self;
//        UIView *droppingView = self.droppingView;
//        self.attachmentBehavior.action = ^{
//            UIBezierPath *path = [UIBezierPath new];
//            [path moveToPoint:weakSelf.attachmentBehavior.anchorPoint];
//            [path addLineToPoint:droppingView.center];
//            weakSelf.gameView.path = path;
//        };
//        
//        self.droppingView = nil;
//        [self.animator addBehavior:self.attachmentBehavior];
//    }
//}

@end
